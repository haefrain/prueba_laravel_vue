# Prueba Laravel + VueJS + Metronic

Este proyecto fue realizado con Laravel, VueJS y la plantilla Metronic.

## Instalacion

Lo primero que se debe hacer en clonar el repositorio para ello utilizaremos el siguiente comando.
```bash
git clone https://gitlab.com/haefrain/prueba_laravel_vue.git
```

Se debe tener en cuenta que al clonarlo lo ideal es dejar el folder en un entorno que ejecute PHP, MySQL y NodeJS.

En caso de no disponer de ellos se puede hacer uso de [XAMPP](https://www.apachefriends.org/es/download.html)

y tambien de [NodeJS](https://nodejs.org/es/download/)

Una vez esten instalados y el proyecto clonado procederemos a ingresar al backend y en caso de que no exista crear el archivo .env, esto lo haremos copiando el archivo .env.example.

En el haremos la configuracion de los campos de la base de datos

```bash
DB_CONNECTION=
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```

Una vez configurados debemos ejecutar el siguiente comando en la terminal de [GIT](https://git-scm.com/downloads), en el directorio raiz "Backend"

```bash
composer install
php artisan key:generate
php artisan migrate --seed
```

Muy bien ya hemos instalado los paquetes de laravel *Composer Install*, hemos generado una key unica para el proyecto de laravel *php artisan key:generate* y finalmente hemos ejecutado las migraciones con los seeders *php artisan migrate --seed*

Ahora debemos desde la terminal dirigirnos a la carpeta Frontend que esta en el mismo nivel de backend.

Una vez alli deberemos ejecutar el siguiente comando.

```bash
npm install
```

Este proceso tardara un poco pero una vez ejecutado tendremos todos nuestros paquetes de Metronic instalados.

Ahora debemos cambiar la URL a la que apuntan los request del Frontend asi que debemos ir a la siguiente ruta

/frontend/src/core/services/api.service.js

y debemos cambiar la linea 14

```bash
Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL =
      "localhost/prueba_laravel_vue/backend/public";
```

finalmente en el caso de querer utilizar el proyecto en modo produccion se debe cambiar la ruta en donde esta generado el proyecto para ello debemos modificar el siguiente archivo

/frontend/vue.config.js

alli en la linea 5 encontraremos una condicion de entorno alli esta produccion y se podra modificar la ruta.

Finalmente para generar una build del proyecto de produccion debemos ejecutar el siguiente comando.

```bash
npm run build
```

en el caso de querer ejecutar el proyecto en entorno de desarrollo debemos ejecutar el siguiente comando.

```bash
npm run serve
```

Muy bien el proyecto ya se debe estar ejecutando correctamente y debe funcionar bastante bien.

## Informacion adicional

Se dejan activos los logs de request intencionalmente, para que los puedan visualizar.