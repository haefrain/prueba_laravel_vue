<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;

    protected $table = 'users';

    protected $fillable = [
        'first_name', 'last_name', 'email', 'country_id'
    ];


    public $timestamps = true;

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

}
