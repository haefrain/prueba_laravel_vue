<?php

use Illuminate\Database\Seeder;
use App\Country;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::insert([
            [
                'name' => 'Colombia',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'España',
                'created_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
