import Vue from "vue";
import ApiService from "@/core/services/api.service";

// action types
export const GET_ALL_USERS = "getAllUsers";
export const INSERT_USER = "insertUser";
export const EDIT_USER = "editUser";
export const UPDATE_USER = "updateUser";
export const DELETE_USER = "deleteUser";

// mutation types
export const PURGE_AUTH = "logOut";
export const ADD_USERS = "addUsers";
export const SET_USERS = "setUsers";
export const SET_USER = "setUser";
export const SET_USER_UPDATED = "setUserUpdated";
export const REMOVE_USER = "removeUser";
export const CLEAN_EDIT_USER = "cleanEditUser";
export const SET_MESSAGE = "setMessage";

const state = {
  message: {},
  userEdited: {},
  users: []
};

const getters = {
  users(state) {
    return state.users;
  },
  userEdited(state) {
    return state.userEdited;
  },
  message(state) {
    return state.message;
  }
};

const actions = {
  [GET_ALL_USERS](context) {
    return new Promise(resolve => {
      ApiService.get("/user")
        .then(({ data }) => {
          context.commit(SET_USERS, data);
          resolve(data);
        })
        .catch(() => {
          context.commit(SET_MESSAGE, {
            state: "danger",
            message: "No se han podido cargar los usuarios."
          });
        });
    });
  },
  [INSERT_USER](context, dataForm) {
    return new Promise(resolve => {
      ApiService.post("/user", dataForm)
        .then(({ data }) => {
          context.commit(ADD_USERS, data.result);
          context.commit(SET_MESSAGE, data.status);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_MESSAGE, response.status);
        });
    });
  },
  [EDIT_USER](context, data) {
    context.commit(SET_USER, data);
  },
  [UPDATE_USER](context, formData) {
    return new Promise(resolve => {
      ApiService.update("/user", formData.id, formData)
        .then(({ data }) => {
          context.commit(SET_USER_UPDATED, data.result);
          context.commit(SET_MESSAGE, data.status);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_MESSAGE, response.status);
        });
    });
  },
  [DELETE_USER](context, user) {
    ApiService.delete("/user/" + user.id)
      .then(({ data }) => {
        context.commit(REMOVE_USER, user);
        context.commit(SET_MESSAGE, data.status);
      })
      .catch(({ response }) => {
        context.commit(SET_MESSAGE, response.status);
      });
  }
};

const mutations = {
  [ADD_USERS](state, user) {
    state.users.push(user);
  },
  [SET_USERS](state, users) {
    state.users = users;
  },
  [SET_USER](state, user) {
    state.userEdited = user;
  },
  [SET_USER_UPDATED](state, user) {
    const position = state.users.indexOf(state.userEdited);
    Vue.set(state.users, position, user);
  },
  [CLEAN_EDIT_USER](state) {
    state.message = {};
    state.userEdited = {};
  },
  [REMOVE_USER](state, user) {
    if (
      state.userEdited.id &&
      state.userEdited.id === state.users.indexOf(user).id
    ) {
      state.userEdited = {};
    }
    state.users.splice(state.users.indexOf(user), 1);
  },
  [SET_MESSAGE](state, status) {
    if (status === 500) {
      state.message = Object.assign({}, state.message, {
        state: 'danger',
        message: 'Ha ocurrido un error.'
      });
    } else {
      state.message = Object.assign({}, state.message, status);
    }
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};
