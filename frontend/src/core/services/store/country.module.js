import ApiService from "@/core/services/api.service";

// action types
export const GET_ALL_COUNTRIES = "getAllCountries";

// mutation types
export const SET_COUNTRIES = "setCountries";
export const SET_ERROR = "setErrors";

const state = {
  errors: null,
  countries: []
};

const getters = {
  countries(state) {
    return state.countries;
  }
};

const actions = {
  [GET_ALL_COUNTRIES](context) {
    ApiService.get("country")
      .then(data => {
        context.commit(SET_COUNTRIES, data.data);
      })
      .catch(({ response }) => {
        context.commit(SET_ERROR, response.data.errors);
      });
  }
};

const mutations = {
  [SET_COUNTRIES](state, countries) {
    state.countries = countries;
  },
  [SET_ERROR](state, errors) {
    state.errors = errors;
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};
