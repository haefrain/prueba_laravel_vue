<?php

namespace App\Http\Controllers;

use App\User;
use App\Country;
use App\Http\Requests\UserRequestStore;
use App\Http\Requests\UserRequestUpdate;

class UserController extends Controller
{

    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        return $this->user->with('country')->get();
    }

    public function store(UserRequestStore $request)
    {
        try {
            $result = $this->user->create($request->all());
            $result->country = Country::find($request->country_id);
            return response()->json([
                "result" => $result,
                "status" => [
                    "state" => "success",
                    "message" => "Se ha creado el usuario con exito."
                ]
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "result" => null,
                "status" => [
                    "state" => "danger",
                    "message" => $th->data->message
                ]
            ]);
        }

    }

    public function update(UserRequestUpdate $request, $id)
    {
        try {
            $this->user->find($id)->update($request->all());
            return response()->json([
                "result" => $request->all(),
                "status" => [
                    "state" => "success",
                    "message" => "Se ha actualizado el usuario con exito."
                ]
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "result" => null,
                "status" => [
                    "state" => "danger",
                    "message" => $th->data->message
                ]
            ]);
        }


    }

    public function destroy($id)
    {
        if($this->user->destroy($id)) {
            return response()->json([
                "result" => $id,
                "status" => [
                    "state" => "success",
                    "message" => "Se ha eliminado el usuario con exito."
                ]
            ]);
        } else {
            return response()->json([
                "result" => null,
                "status" => [
                    "state" => "danger",
                    "message" => "Ha ocurrido un error al intentar eliminar el usuario"
                ]
            ]);
        }
    }
}
