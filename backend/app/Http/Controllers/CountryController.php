<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;

class CountryController extends Controller
{

    private $country;

    public function __construct(Country $country)
    {
        $this->country = $country;
    }

    public function index()
    {
        return response()->json($this->country->select('id AS value', 'name AS text')->get());
    }
}
